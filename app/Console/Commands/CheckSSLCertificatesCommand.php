<?php

namespace App\Console\Commands;

use App\Jobs\CheckSSLCertificateJob;
use Illuminate\Console\Command;
use App\Models\Site;

class CheckSSLCertificatesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:check-ssl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check SSL certificates for all domains';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $chunkSize = 10;

        Site::orderBy('id')->chunk($chunkSize, function ($sites) {
            foreach ($sites as $site) {
                CheckSSLCertificateJob::dispatch($site);
            }
        });
    }
}
