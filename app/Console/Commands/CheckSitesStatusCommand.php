<?php

namespace App\Console\Commands;

use App\Jobs\CheckSiteStatusJob;
use App\Models\Site;
use Illuminate\Console\Command;

class CheckSitesStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sites:check-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check the status of all sites';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $chunkSize = 10;

        Site::orderBy('id')->chunk($chunkSize, function ($sites) {
            foreach ($sites as $site) {
                CheckSiteStatusJob::dispatch($site);
            }
        });
    }
}
