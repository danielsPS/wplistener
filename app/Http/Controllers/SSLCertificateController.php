<?php

namespace App\Http\Controllers;

use App\Jobs\CheckSSLCertificateJob;
use App\Models\Site;
use App\Models\Tag;
use App\Services\SiteFilterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SSLCertificateController extends Controller
{
    public function index(Request $request, SiteFilterService $filterService)
    {
        $query = Site::where('user_id', Auth::id())->where('services_ssl', true);
        $query = $filterService->filter($query, $request);
        $sites = $query->get();

        $tags = Tag::where('user_id', Auth::id())->get();
        $groups = Site::where('user_id', Auth::id())->select('group')->distinct()->pluck('group');

        return view('ssl', compact('sites', 'request', 'tags', 'groups'));
    }

    public function check()
    {
        $userSites = Site::where('user_id', Auth::id())->get();

        foreach ($userSites as $site) {
            CheckSSLCertificateJob::dispatch($site);
        }

        return back()->with('success', 'SSL certificate check has been initiated. Please note that it might take a few minutes for the updated data to be reflected.');
    }
}
