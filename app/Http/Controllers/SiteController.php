<?php

namespace App\Http\Controllers;

use App\Models\Site;
use App\Models\Tag;
use App\Rules\ValidDomain;
use App\Services\SiteFilterService;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\MessageBag;

class SiteController extends Controller
{
    public function index(Request $request, SiteFilterService $filterService)
    {
        return $this->manage($request, $filterService);
    }

    public function manage(Request $request, SiteFilterService $filterService)
    {
        $query = Site::where('user_id', Auth::id());
        $query = $filterService->filter($query, $request);
        $sites = $query->get();

        $tags = Tag::where('user_id', Auth::id())->get();
        $groups = Site::where('user_id', Auth::id())
            ->whereNotNull('group')
            ->distinct()
            ->pluck('group');

        return view('sites.manage', compact('sites', 'request', 'groups', 'tags'));
    }

    public function create()
    {
        $tags = Tag::where('user_id', Auth::id())->get();
        $groups = Site::where('user_id', Auth::id())
            ->whereNotNull('group')
            ->distinct()
            ->pluck('group');

        return view('sites.create', compact('tags', 'groups'));
    }

    public function edit($id)
    {
        $site = Site::findOrFail($id);
        $groups = Site::where('user_id', Auth::id())
            ->whereNotNull('group')
            ->distinct()
            ->pluck('group');
        $tags = Tag::where('user_id', Auth::id())->get();

        return view('sites.edit', compact('site', 'groups', 'tags'));
    }

    public function delete($id){
        $site = Site::findOrFail($id);

        return view('sites.delete', compact('site'));
    }

    public function import()
    {
        return view('sites.import');
    }

    public function store(Request $request)
    {
        $site = new Site;
        $site->user_id = Auth::id();
        $response = $this->saveSite($site, $request);

        // Check if saveSite returned a response (indicating an error)
        if ($response instanceof RedirectResponse) {
            return $response; // Return the error response
        }

        return redirect()->route('sites.manage')->with('success', 'Site added successfully.');
    }

    public function update(Request $request, $id)
    {
        $site = Site::findOrFail($id);

        // Authorization check: Ensure the authenticated user owns the site
        if ($site->user_id !== Auth::id()) {
            abort(403, 'Unauthorized action.');
        }

        $response = $this->saveSite($site, $request);

        // Check if saveSite returned a response (indicating an error)
        if ($response instanceof RedirectResponse) {
            return $response; // Return the error response
        }

        return redirect()->route('sites.manage')->with('success', 'Site updated successfully.');
    }

    private function saveSite(Site $site, Request $request)
    {
        // Validate input fields
        $validatedData = $request->validate([
            'site_name' => 'required|max:30',
            'site_url' => 'required|url',
        ], [
            'site_name.required' => 'A site name is required.',
            'site_name.max' => 'The site name may not be greater than 30 characters.',
            'site_url.required' => 'A URL is required.',
            'site_url.url' => 'The URL must be valid.',
        ]);

        // Check for duplicate URL
        $duplicateSiteValidationQuery = Site::where('user_id', Auth::id())
            ->where('url', $request->input('site_url'));

        if (isset($site->id)) {
            // Exclude the current site if updating
            $duplicateSiteValidationQuery->where('id', '<>', $site->id);
        }

        if ($duplicateSiteValidationQuery->first()) {
            $errors = new MessageBag();
            $errors->add('site_url', 'A site with this URL already exists.');

            return back()->withErrors($errors)->withInput();
        }

        // Process the site_url to extract the domain
        $parsedUrl = parse_url($validatedData['site_url']);
        $domain = $parsedUrl['host'] ?? $parsedUrl['path'];

        // Set site properties
        $site->name = $validatedData['site_name'];
        $site->url = $validatedData['site_url'];
        $site->domain = $domain;
        $site->group = $request->input('site_group', '');
        $site->services_uptime = $request->has('services_uptime');
        $site->services_ssl = $request->has('services_ssl');

        $site->save();

        // Handle tags
        $tagIds = collect($request->input('site_tags', []))->map(function ($tagName) {
            return Tag::firstOrCreate([
                'user_id' => Auth::id(),
                'name' => $tagName
            ])->id;
        });

        $site->tags()->sync($tagIds);

        return true;
    }

    public function destroy(Site $site)
    {
        // Check if the authenticated user owns the site
        if (auth()->id() !== $site->user_id) {
            abort(403, 'Unauthorized action.');
        }

        // Delete the site
        $deleted = $site->delete();

        return redirect()->route('sites.manage')->with(
            $deleted ? 'success' : 'error',
            $deleted ? 'Site deleted successfully.' : 'There was an issue deleting the site.'
        );
    }
}
