<?php

namespace App\Http\Controllers;

use App\Jobs\CheckSiteStatusJob;
use App\Models\Site;
use App\Models\Tag;
use App\Services\SiteFilterService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UptimeController extends Controller
{
    public function index(Request $request, SiteFilterService $filterService)
    {
        $query = Site::where('user_id', Auth::id())->where('services_uptime', true);
        $query = $filterService->filter($query, $request);
        $sites = $query->get();

        $tags = Tag::where('user_id', Auth::id())->get();

        return view('uptime', compact('sites', 'request', 'tags'));
    }

    public function check()
    {
        $userSites = Site::where('user_id', Auth::id())->get();

        foreach ($userSites as $site) {
            CheckSiteStatusJob::dispatch($site);
        }

        return back()->with('success', 'Site status check has been initiated. Please note that it might take a few minutes for the updated data to be reflected.');
    }
}
