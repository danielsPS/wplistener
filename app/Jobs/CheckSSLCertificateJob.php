<?php

namespace App\Jobs;

use App\Models\Site;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class CheckSSLCertificateJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $site;

    /**
     * Create a new job instance.
     */
    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    /**
     * Execute the job.
     */
    public function handle()
    {
        Log::info('Checking site SSL certificate', ['domain' => $this->site->domain]);

        $streamContext = stream_context_create([
            "ssl" => [
                "capture_peer_cert" => TRUE,
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ]);

        try {
            $read = stream_socket_client("ssl://" . parse_url($this->site->url, PHP_URL_HOST) . ":" . (parse_url($this->site->url, PHP_URL_PORT) ?? 443), $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $streamContext);

            if ($read) {
                $cert = stream_context_get_params($read);
                $certDetails = openssl_x509_parse($cert['options']['ssl']['peer_certificate']);

                $this->site->sslCertificate()->updateOrCreate(
                    ['site_id' => $this->site->id],
                    [
                        'expiry_date' => date('Y-m-d', $certDetails['validTo_time_t']),
                        'issuer' => $certDetails['issuer']['O'] ?? __('Unknown Issuer')
                    ]
                );
            }

        } catch (\Exception $e) {
            Log::error('Failed to retrieve SSL details', ['domain' => $this->site->domain, 'error' => $e->getMessage()]);
        }
    }

}
