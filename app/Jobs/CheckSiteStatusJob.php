<?php

namespace App\Jobs;

use App\Models\Downtime;
use App\Models\ResponseTime;
use App\Models\Site;
use App\Models\Uptime;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Exception;
use Illuminate\Support\Facades\Log;

class CheckSiteStatusJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected Site $site;

    /**
     * Create a new job instance.
     */
    public function __construct(Site $site)
    {
        $this->site = $site;
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        try {
            Log::info('Checking site status', ['domain' => $this->site->domain]);

            $responseTimeStart = microtime(true);
            $response = Http::timeout(10)->get($this->site->domain);
            $responseTimeEnd = microtime(true);
            $responseTime = $responseTimeEnd - $responseTimeStart;

            if ($response->successful()) {
                $this->logResponseTime($this->site, $responseTime);
                $this->updateUptime($this->site, true);
                $this->resolveDowntime($this->site);
            } else {
                $this->updateUptime($this->site, false);
                $this->logDowntime($this->site);
            }
        } catch (\Illuminate\Http\Client\ConnectionException $e) {
            Log::error('Site check failed: Connection problem', ['domain' => $this->site->domain, 'error' => $e->getMessage()]);
            $this->updateUptime($this->site, false);
            $this->logDowntime($this->site);
        } catch (Exception $e) {
            Log::error('Site check failed: General exception', ['domain' => $this->site->domain, 'error' => $e->getMessage()]);
        }
    }

    /**
     *  Update uptime data
     */
    protected function updateUptime($site, $status)
    {
        Uptime::updateOrCreate(
            ['site_id' => $site->id],
            [
                'site_id' => $site->id,
                'status' => $status
            ]
        );
    }

    /**
     *  Start downtime instance
     */
    protected function logDowntime($site)
    {
        $existingDowntime = Downtime::where('site_id', $site->id)
            ->whereNull('end_time')
            ->latest()
            ->first();

        if ($existingDowntime === null) {
            Downtime::create([
                'site_id' => $site->id,
                'start_time' => now()
            ]);
        }
    }

    /**
     *  End downtime instance
     */
    protected function resolveDowntime($site)
    {
        $downtime = Downtime::where('site_id', $site->id)
            ->whereNull('end_time')
            ->latest()
            ->first();

        $downtime?->update(['end_time' => now()]);
    }

    /**
     * Log the response time
     */
    protected function logResponseTime($site, $responseTime)
    {
        // Delete response times older than 24 hours for this site
        ResponseTime::where('site_id', $site->id)
            ->where('created_at', '<', now()->subDay())
            ->delete();

        // Create a new response time record
        ResponseTime::create([
            'site_id' => $site->id,
            'response_time' => $responseTime
        ]);
    }
}
