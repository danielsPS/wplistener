<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Downtime extends Model
{
    use HasFactory;

    protected $fillable = ['start_time', 'end_time', 'site_id'];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
