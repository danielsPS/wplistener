<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SSLCertificate extends Model
{
    use HasFactory;

    protected $fillable = ['expiry_date', 'issuer', 'site_id'];
    protected $table = 'ssl_certificates';

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
