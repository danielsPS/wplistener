<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Integer;

class Site extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'name',
        'domain',
        'url',
        'group',
        'uptime',
        'ssl'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sslCertificate()
    {
        return $this->hasOne(SSLCertificate::class);
    }

    public function uptime()
    {
        return $this->hasOne(Uptime::class);
    }

    public function downtimes()
    {
        return $this->hasMany(Downtime::class);
    }

    public function responseTimes()
    {
        return $this->hasMany(ResponseTime::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'site_tag');
    }


    /**
     * Get total monitored time in seconds
     *
     * @return float|int
     */
    public function getTotalMonitoredTime(): float|int
    {
        $uptime = $this->uptime;
        return $uptime ? now()->diffInSeconds($uptime->created_at) : 0;
    }


    /**
     * Get total downtime in seconds
     *
     * @return float|int
     */
    public function getTotalDowntime():  float|int
    {
        return $this->downtimes->reduce(function ($carry, $downtime) {
            $endTime = $downtime->end_time ?? now();

            if (!$endTime instanceof Carbon) {
                $endTime = new Carbon($endTime);
            }

            return $carry + $endTime->diffInSeconds($downtime->start_time);
        }, 0);
    }


    /**
     * Get the formatted availability/uptime percentage relative to the total time
     *
     * @return string
     */
    public function getFormattedAvailability(): string
    {
        $totalMonitoredTime = $this->getTotalMonitoredTime();
        if ($totalMonitoredTime === 0) return '0%';

        $totalDowntime = $this->getTotalDowntime();
        $uptime = $totalMonitoredTime - $totalDowntime;
        $availability = ($uptime / $totalMonitoredTime) * 100;

        return round($availability) . '%';
    }

    /**
     * Get the formatted downtime in minutes and percentage relative to the total time
     *
     * @return string
     */
    public function getFormattedDowntime(): string
    {
        $totalDowntime = $this->getTotalDowntime();
        $hours = floor($totalDowntime / 3600);
        $minutes = floor(($totalDowntime % 3600) / 60);

        $downtimeString = $hours > 0 ? "{$hours} h " : "";
        $downtimeString .= "{$minutes} min";

        $totalMonitoredTime = $this->getTotalMonitoredTime();
        $downtimePercentage = $totalMonitoredTime > 0 ? round(($totalDowntime / $totalMonitoredTime) * 100) : 0;

        return "{$downtimeString} ({$downtimePercentage}%)";
    }

    /**
     * Get uptime status for each hour in last 24 hours
     *
     * @return array
     */
    public function getUptimeStatusForLast24Hours(): array
    {
        $statuses = [];
        for ($i = 23; $i >= 0; $i--) {
            $start = now()->subHours($i + 1);
            $end = now()->subHours($i);
            $wasDown = Downtime::where('site_id', $this->id)
                ->whereBetween('start_time', [$start, $end])
                ->orWhere(function ($query) use ($start, $end) {
                    $query->where('site_id', $this->id)
                        ->whereNull('end_time')
                        ->where('start_time', '<', $end);
                })
                ->exists();
            $statuses[] = !$wasDown;
        }
        return $statuses;
    }


    /**
     * Get the formatted average response time in milliseconds
     *
     * @return string
     */
    public function getFormattedAverageResponseTime(): string
    {
        $averageResponseTimeInSeconds = $this->responseTimes()->avg('response_time');
        $averageResponseTimeInMilliseconds = round($averageResponseTimeInSeconds * 1000);

        return "{$averageResponseTimeInMilliseconds} " . __('ms');
    }

    /**
     * Get average response time for each hour in last 24 hours
     *
     * @return array
     */
    public function getAverageResponseTimeForLast24Hours(): array
    {
        $responseTimes = [];
        for ($i = 23; $i >= 0; $i--) {
            $start = now()->subHours($i + 1);
            $end = now()->subHours($i);
            $average = ResponseTime::where('site_id', $this->id)
                ->whereBetween('created_at', [$start, $end])
                ->avg('response_time');
            $responseTimes[] = $average ?: 0;
        }
        return $responseTimes;
    }

    /**
     * For testing - get average response time for each hour in last 24 hours
     *
     * @return array
     */
    public function getMockAverageResponseTimeForLast24Hours(): array
    {
        $responseTimes = [];
        for ($i = 0; $i < 24; $i++) {
            $responseTimes[] = mt_rand(0, 10000) / 10000;
        }
        return $responseTimes;
    }

    /**
     * Get the formatted average response time in milliseconds.
     *
     * @return string
     */
    public function getDaysUntilSslExpiryDate(): string
    {
        $expiryDate = Carbon::parse($this->sslCertificate->expiry_date);
        return Carbon::now()->diffInDays($expiryDate, false);
    }
}
