<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Uptime extends Model
{
    use HasFactory;

    protected $fillable = ['status', 'site_id'];

    public function site()
    {
        return $this->belongsTo(Site::class);
    }
}
