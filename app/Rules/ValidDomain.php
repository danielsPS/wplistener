<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use App\Models\Site;

class ValidDomain implements Rule
{
    private $errorMessage = '';

    public function passes($attribute, $value): bool
    {
        // Check if input is a URL format
        $parsedUrl = parse_url('https://' . $value); // Prepend with http:// to ensure proper parsing
        if ($parsedUrl['host'] !== $value) {
            $this->errorMessage = 'The :attribute should be a domain name only, not a URL.';
            return false;
        }


        // Check if it's a valid domain format
        if (!filter_var($value, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME)) {
            $this->errorMessage = 'The :attribute is not a valid domain format.';
            return false;
        }

        // Ensure the domain includes a top-level domain
        if (!preg_match('/\.[a-zA-Z]{2,}$/', $value)) {
            $this->errorMessage = 'The :attribute must include a top-level domain like .com, .net, etc.';
            return false;
        }

        // Check for domain existence
        if (Site::where('domain', $value)
            ->where('user_id', auth()->id())
            ->exists()) {
            $this->errorMessage = 'The :attribute already exists.';
            return false;
        }

        return true;
    }

    public function message(): string
    {
        return $this->errorMessage;
    }
}
