<?php
namespace App\Services;

use Illuminate\Http\Request;

class SiteFilterService
{
    public function filter($query, Request $request)
    {
        if ($request->filled('site_url')) {
            $query->where('url', 'like', '%' . $request->site_url . '%');
        }

        if ($request->filled('site_group')) {
            $query->where('group', $request->site_group);
        }

        if ($request->filled('site_tags')) {
            $query->whereHas('tags', function ($q) use ($request) {
                $q->where('tags.id', $request->site_tags);
            });
        }

        return $query;
    }
}
