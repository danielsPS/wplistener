<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class ImageComponent extends Component
{
    public $src;
    public $alt;
    public $height;
    public $width;
    public $wrapperClass;
    public $href;

    /**
     * Create a new component instance.
     */
    public function __construct($src, $alt = '', $height = null, $width = null, $wrapperClass = '', $href = null)
    {
        $this->src = $src;
        $this->alt = $alt;
        $this->height = $height;
        $this->width = $width;
        $this->wrapperClass = $wrapperClass;
        $this->href = $href;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.image-component');
    }
}
