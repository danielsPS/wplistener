<?php

// Set the time limit to 5 hour
set_time_limit(18000);

// Start time
$start_time = time();

// Path to your Laravel project
$projectPath = '/Users/ps/PhpstormProjects/wplistener';

while (true) {
    // Call Laravel's artisan command
    exec("bash -c 'cd {$projectPath} && ./vendor/bin/sail artisan sites:check-status'");

    // Wait for 5 minutes (300 seconds)
    sleep(60);

    // Check if an 5 hours has passed
    if (time() - $start_time >= 18000) {
        break;
    }
}
