<?php

namespace Database\Factories;

use App\Models\ResponseTime;
use Illuminate\Database\Eloquent\Factories\Factory;

class ResponseTimeFactory extends Factory
{
    protected $model = ResponseTime::class;

    public function definition()
    {
        return [
            'site_id' => 1,
            'response_time' => $this->faker->randomFloat(4, 0, 1),
            'created_at' => now()->subHours(rand(0, 23)),
            'updated_at' => now()
        ];
    }
}
