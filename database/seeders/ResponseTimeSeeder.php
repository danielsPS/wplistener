<?php

namespace Database\Seeders;

use App\Models\ResponseTime;
use Illuminate\Database\Seeder;

class ResponseTimeSeeder extends Seeder
{
    public function run()
    {
        ResponseTime::where('site_id', 1)->delete();

        ResponseTime::factory(4000)->create();
    }
}
