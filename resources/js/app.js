import './bootstrap';
import $ from 'jquery';
import Alpine from 'alpinejs';
import Chart from 'chart.js/auto';
window.$ = window.jQuery = $;
import select2 from 'select2';
select2();

window.Alpine = Alpine;
Alpine.start();

import './select.js';
import './profile.js';
import './uptime.js';
