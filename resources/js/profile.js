import $ from "jquery";

$(document).ready(function() {
    const $profilePictureImg = $('.profile-picture img');
    const $defaultPicture = $('#default_picture');
    const $removePictureCheckbox = $('#remove_picture');
    const $profilePictureInput = $('#profile_picture');

    function updateProfilePicture(src) {
        $profilePictureImg.attr('src', src);
    }

    $('.js-add-profile-picture').click(function() {
        $profilePictureInput.click();
    });

    $('.js-remove-profile-picture').click(function() {
        updateProfilePicture($defaultPicture.val());
        $removePictureCheckbox.prop('checked', true);
    });

    $profilePictureInput.change(function() {
        const file = this.files[0];
        if (file) {
            updateProfilePicture(window.URL.createObjectURL(file));
            $removePictureCheckbox.prop('checked', false);
        }
    });
});

