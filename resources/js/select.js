import $ from "jquery";

String.prototype.toColor = function() {
    const colors = [
        "#FF947A",
        "#3CD856",
        "#5A6ACF",
        "#673ab7",
        "#3f51b5",
        "#5677fc",
        "#03a9f4",
        "#00bcd4",
        "#009688",
        "#259b24",
        "#8bc34a",
        "#afb42b",
        "#ff9800",
        "#ff5722",
        "#795548",
        "#607d8b"
    ]

    let hash = 0;
    if (this.length === 0) return hash;
    for (let i = 0; i < this.length; i++) {
        hash = this.charCodeAt(i) + ((hash << 5) - hash);
        hash = hash & hash;
    }
    hash = ((hash % colors.length) + colors.length) % colors.length;
    return colors[hash];
}


$(document).ready(function() {
    $('.select2').select2();

    $('.select2-tags').select2({
        tags: true
    }).on('change', function() {
        const $selected = $(this).find('option:selected');
        const $container = $(this).siblings('.js-tag-display-area');

        if($selected.length <= 0){
            $container.html('');
            return;
        }

        const $list = $('<ul>');

        $selected.each(function(k, v) {
            const text = $(this).text();
            const value = $(this).val();

            const $li = $('<li class="tag" style="background-color:' + text.toColor() + '">' + text + '<button class="remove-tag" data-tag-value="' + value + '">×</button></li>');

            $li.children('.remove-tag')
                .off('click.select2-copy')
                .on('click.select2-copy', function(e) {
                    e.preventDefault();

                    const $opt = $(this).data('select2-opt');
                    $opt.prop('selected', false);
                    $opt.parents('select').trigger('change');

                }).data('select2-opt', $(v));

            $list.append($li);
        });

        $container.html('').append($list);

    }).trigger('change');
});
