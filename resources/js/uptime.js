import Chart from 'chart.js/auto';

window.loadChart = function(chartId, chartData) {
    const ctx = document.getElementById(chartId).getContext('2d');
    console.log(chartData)
    new Chart(ctx, {
        type: 'line',
        data: {
            labels: chartData.labels,
            datasets: [{
                label: chartData.label,
                data: chartData.data,
                borderColor: '#5A6ACF',
                borderWidth: 2,
                tension: 0.1,
                pointRadius: 0,
                pointHitRadius: 0,
                pointHoverRadius: 0,
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    suggestedMin: 0,
                    display: false,
                    grid: {
                        display: false,
                    },
                    ticks: {
                        display: false
                    }
                },
                x: {
                    display: false,
                    grid: {
                        display: false,
                    },
                    ticks: {
                        display: false
                    }
                }
            },
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    enabled: false
                }
            },
        }
    });
};
