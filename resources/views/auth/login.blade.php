<x-guest-layout>
    <h2>{{__('Nice to see you again')}}</h2>

    <!-- Session Status -->
    <x-auth-session-status class="mb-4" :status="session('status')" />

    <form method="POST" action="{{ route('login') }}">
        @csrf

        <!-- Email Address -->
        <div class="input-wrapper">
            <x-input-label for="email" :value="__('Login')" />
            <x-text-input id="email" type="email" name="email" :value="old('email')" placeholder="{{__('Email or phone number')}}" required autofocus autocomplete="username" />
        </div>

        <!-- Password -->
        <div class="input-wrapper">
            <x-input-label for="password" :value="__('Password')" />
            <x-text-input id="password" type="password" name="password" placeholder="{{__('Enter password')}}" required autocomplete="current-password" />
        </div>


        <div class="remember-me-section flex items-center justify-between">
            <!-- Remember Me -->
            <label for="remember_me" class="checkbox-wrapper">
                <div class="checkbox-slider-wrapper">
                    <input id="remember_me" type="checkbox" name="remember">
                    <span class="checkbox-slider checkbox-slider-round"></span>
                </div>
                <p class="checkbox-label">{{ __('Remember me') }}</p>
            </label>

            <!-- Forgot password -->
            @if (Route::has('password.request'))
                <a class="" href="{{ route('password.request') }}">
                    {{ __('Forgot password?') }}
                </a>
            @endif
        </div>

        <div class="login-button-wrapper">
            <x-primary-button class="w-full">
                {{ __('Sign in') }}
            </x-primary-button>
        </div>

        <!-- Google Login Button -->
        <div class="login-with-google-button-wrapper ">
            <a href="{{ route('login.google') }}" class="primary-button w-full inline-flex items-center justify-center text-white transition ease-in-out duration-150">
                <x-image-component src="{{ asset('assets/icons/logos/google.svg') }}" alt="{{ __('sign in with Google') }}" height="20" width="20" />
                <span>{{ __('Or sign in with Google') }}</span>
            </a>
        </div>

        <!-- Sign Up Section -->
        <div class="login-form-register-link-wrapper text-center flex items-center justify-center flex-wrap w-full">
            <span class="">{{ __('Don\'t have an account?') }}</span>
            <a href="{{ route('register') }}" class="">
                {{ __('Sign up now') }}
            </a>
        </div>
    </form>
</x-guest-layout>
