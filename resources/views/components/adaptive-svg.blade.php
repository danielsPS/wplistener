@if($url)
    <div class="svg duration-100 ease-linear" style="
        -webkit-mask-image: url('{{ $url }}');
        mask-image: url('{{ $url }}');
    @if($height) height:{{ $height }}px; @endif
    @if($width) width:{{ $width }}px; @endif
        "></div>
@endif

