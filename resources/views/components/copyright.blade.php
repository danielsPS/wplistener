<div class="copyright-footer">
    <div class="developed-by-text-wrapper flex items-center">
        <p>{{__('Developed by')}}</p>
        <x-image-component
            src="{{ asset('assets/icons/logos/polite-pixel-logo.svg') }}"
            alt="{{ __('Polite Pixel') }}"
            height="18"
            width="33"
            href="https://politepixel.com/"
        />
    </div>

    <div class="copyright-message-wrapper">
        <p>{{__('©All Rights Reserved')}}</p>
    </div>
</div>
