<div class="image-wrapper flex justify-center {{ $wrapperClass }}">
    @if($href)
        <a href="{{ $href }}">
            <img src="{{ $src }}" alt="{{ $alt }}" @if($height) height="{{ $height }}" @endif @if($width) width="{{ $width }}" @endif>
        </a>
    @else
        <img src="{{ $src }}" alt="{{ $alt }}" @if($height) height="{{ $height }}" @endif @if($width) width="{{ $width }}" @endif>
    @endif
</div>
