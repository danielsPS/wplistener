@props(['active'])

@php
$classes = ($active ?? false)
            ? 'active inline-flex items-center transition duration-100 ease-linear'
            : 'inline-flex items-center transition duration-100 ease-linear';
@endphp

<a {{ $attributes->merge(['class' => $classes]) }}>
    {{ $slot }}
</a>
