{{-- Check for success or error message --}}
@if (session('success') || $errors->any() || session('error') || session('status'))
    <div class="notifications-section">
        {{-- Success Message --}}
        @if (session('success'))
            <div class="alert alert-success">
                <ul>
                    <li>{{ session('success') }}</li>
                </ul>
            </div>
        @endif


        @if (session('status'))
            <div class="alert alert-success">
                <ul>
                    @if (session('status') === 'profile-updated')
                        <li>{{ __('Profile Updated') }}</li>
                    @elseif(session('status') === 'password-updated')
                        <li>{{ __('Password Updated') }}</li>
                    @elseif(session('status') === 'verification-link-sent')
                        <li>{{ __('A new verification link has been sent to your email address.') }}</li>
                    @endif
                </ul>
            </div>
        @endif

        {{-- Error Messages --}}
        @if ($errors->any() || session('error'))
            <div class="alert alert-error">
                <ul>
                    {{-- Session Error --}}
                    @if (session('error'))
                        <li>{{ session('error') }}</li>
                    @endif

                    {{-- Validation Errors --}}
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
@endif
