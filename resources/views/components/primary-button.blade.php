<button {{ $attributes->merge(['type' => 'submit', 'class' => 'primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150']) }}>
    {{ $slot }}
</button>
