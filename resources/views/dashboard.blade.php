@php
    use Carbon\Carbon;
@endphp

<x-app-layout>
    <div class="manage-sites-section">
        {{-- Header--}}
        <header class="accounts-page-header">
            <h2>{{ __('Sites') }}</h2>
            @include('components.notifications')
        </header>

        {{-- Page navigation --}}
        <div class="page-navigation">
            <x-nav-link :href="route('sites.manage')" :active="request()->routeIs('sites.manage')" class="page-navigation-red">
                <x-image-component src="{{ asset('assets/icons/navigation/manage-sites.svg') }}" alt="{{ __('Manage sites') }}" height="35" width="35"/>
                <span>{{ __('Manage sites') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('sites.create')" :active="request()->routeIs('sites.create')" class="page-navigation-green">
                <x-image-component src="{{ asset('assets/icons/navigation/add-new.svg') }}" alt="{{ __('Add new') }}" height="35" width="35"/>
                <span>{{ __('Add new') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('sites.import')" :active="request()->routeIs('sites.import')" class="page-navigation-blue">
                <x-image-component src="{{ asset('assets/icons/navigation/import-sites.svg') }}" alt="{{ __('Import sites') }}" height="38" width="38"/>
                <span>{{ __('Import sites') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('uptime.manage')" class="page-navigation-orange">
                <x-image-component src="{{ asset('assets/icons/navigation/monitoring.svg') }}" alt="{{ __('Monitoring') }}" height="38" width="38"/>
                <span>{{ __('Monitoring') }}</span>
            </x-nav-link>
        </div>

        {{-- Sections --}}
        <div class="sites-sections">
            @yield('content')
        </div>

        {{-- Data overview --}}
        <div class="data-overview-section">
            <div class="section-head">
                <h2>{{__('Data')}}</h2>
                <p>{{__('Wplistener today performance')}}</p>
            </div>

            <div class="performance-1">
                <x-image-component
                    src="{{ asset('assets/images/test/performance_1.jpg') }}"
                    height="233"
                    width="276"
                />
            </div>

            <div class="performance-2">
                <x-image-component
                    src="{{ asset('assets/images/test/performance_2.jpg') }}"
                    height="233"
                    width="276"
                />
            </div>

            <div class="performance-3">
                <x-image-component
                    src="{{ asset('assets/images/test/performance_3.jpg') }}"
                    height="322"
                    width="276"
                />
            </div>
        </div>
    </div>
</x-app-layout>
