<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link rel="icon" href="{{ asset('assets/icons/logos/favicon.ico') }}" type="image/x-icon">

        <title>{{ config('app.name', 'Wplistener') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />

        <!-- Scripts -->
        @vite(['resources/css/app.scss', 'resources/js/app.js'])
    </head>
    <body class="guest-page">
        <div class="guest-page-container min-h-screen flex flex-col items-center">
            <x-image-component
                src="{{ asset('assets/icons/logos/WPlistener_white.svg') }}"
                alt="{{ __('Wplistener') }}"
                height="23"
                width="136"
                wrapper-class="guest-page-logo"
                href="/"
            />
            <div class="guest-form-container bg-white">
                @include('components.notifications')

                {{ $slot }}
            </div>

            @include('components.copyright')

            <x-image-component
                src="{{ asset('assets/icons/backgrounds/login-bg-1.svg') }}"
                alt=""
                height="0"
                width="0"
                wrapper-class="guest-page-bg-1"
            />
            <x-image-component
                src="{{ asset('assets/icons/backgrounds/login-bg-2.svg') }}"
                alt=""
                height="0"
                width="0"
                wrapper-class="guest-page-bg-2"
            />
        </div>
    </body>
</html>
