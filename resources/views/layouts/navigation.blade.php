<!-- Primary Navigation Menu -->
<nav x-data="{ open: false }" class="main-navigation-wrapper min-h-screen">
    <div class="main-navigation flex flex-col items-center">
        <!-- Logo -->
        <x-image-component
            src="{{ asset('assets/icons/logos/WPlistener_white.svg') }}"
            alt="{{ __('Wplistener') }}"
            height="23"
            width="136"
            wrapper-class="navigation-logo"
            href="{{ route('dashboard') }}"
        />

        <!-- Profile Overview / Welcome message-->
        @if (Auth::check())
            <div class="profile-info">
                @if(Auth::user()->profile_picture)
                    <x-image-component
                        src="{{ asset('storage/' . Auth::user()->profile_picture) }}"
                        height="105"
                        width="105"
                    />
                @else
                    <x-image-component
                        src="{{ asset('assets/images/user.png') }}"
                        height="105"
                        width="105"
                    />
                @endif

                <div class="profile-info-name text-center">
                    <h3>
                        <span class="welcome-message block">{{__('Welcome Back,')}}</span>
                        <span>{{ Auth::user()->name }}</span>
                    </h3>
                </div>
            </div>
        @endif

        <!-- Navigation Links -->
        <div class="main-nav-menu flex flex-col">
            <x-nav-link :href="route('dashboard')" :active="request()->routeIs('dashboard') || request()->routeIs('sites.*')">
                <x-adaptive-svg url="{{asset('assets/icons/navigation/sites.svg')}}" height="22" width="22" />
                <span>{{ __('Sites') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('uptime.manage')" :active="request()->routeIs('uptime.manage')">
                <x-adaptive-svg url="{{asset('assets/icons/navigation/uptime.svg')}}" />
                <span>{{ __('Uptime') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('ssl.manage')" :active="request()->routeIs('ssl.manage')">
                <x-adaptive-svg url="{{asset('assets/icons/navigation/ssl-health.svg')}}" />
                <span>{{ __('SSL Health') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('profile.edit')" :active="request()->routeIs('wp_security')">
                <x-adaptive-svg url="{{asset('assets/icons/navigation/wp-security.svg')}}" width="27" height="27" />
                <span>{{ __('WP Security') }}</span>
            </x-nav-link>

            <x-nav-link :href="route('profile.edit')" :active="request()->routeIs('profile.edit')">
                <x-adaptive-svg url="{{asset('assets/icons/navigation/settings.svg')}}" width="23" height="23" />
                <span>{{ __('Settings') }}</span>
            </x-nav-link>

            <form method="POST" action="{{ route('logout') }}">
                @csrf

                <x-nav-link :href="route('logout')" onclick="event.preventDefault(); this.closest('form').submit();">
                    <x-adaptive-svg url="{{asset('assets/icons/navigation/log-out.svg')}}" width="26" height="26" />
                    <span>{{ __('Log out') }}</span>
                </x-nav-link>
            </form>
        </div>

        <!-- Copyright -->
        @include('components.copyright')
    </div>
</nav>
