<x-app-layout>
    <div class="profile-section default-section-layout">
        {{-- Header--}}
        <header class="accounts-page-header">
            <h2>{{ __('Profile') }}</h2>
            @include('components.notifications')
        </header>

        @include('profile.partials.update-profile-information-form')
        @include('profile.partials.update-password-form')
        @include('profile.partials.delete-user-form')
    </div>
</x-app-layout>
