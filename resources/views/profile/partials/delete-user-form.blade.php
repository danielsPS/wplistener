<section class="delete-account-section">
    <header>
        <h2>{{ __('Delete Account') }}</h2>
        <p>
            {{ __('Once your account is deleted, all of its resources and data will be permanently deleted.') }}
        </p>
    </header>

    <x-danger-button
        x-data=""
        x-on:click.prevent="$dispatch('open-modal', 'confirm-user-deletion')"
    >{{ __('Delete Account') }}</x-danger-button>

    <x-modal name="confirm-user-deletion" :show="$errors->userDeletion->isNotEmpty()" focusable>
        <div class="modal-content">
            <div class="modal-header">
                <h2>
                    {{ __('Are you sure you want to delete your account?') }}
                </h2>

                <p>
                    {{ __('Once your account is deleted, all of its resources and data will be permanently deleted. Please enter your password to confirm you would like to permanently delete your account.') }}
                </p>
            </div>

            <form method="post" action="{{ route('profile.destroy') }}" class="default-form">
                @csrf
                @method('delete')

                <div class="input-wrapper">
                    <x-input-label for="password" value="{{ __('Password') }}" class="sr-only" />
                    <x-text-input
                        id="password"
                        name="password"
                        type="password"
                        class="mt-1 block w-3/4"
                        placeholder="{{ __('Password') }}"
                    />
                    <x-input-error :messages="$errors->userDeletion->get('password')" class="mt-2" />
                </div>

                <div class="button-wrapper">
                    <x-primary-button x-on:click="$dispatch('close')" type="button" >
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/arrow-left.png') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Cancel') }}</span>
                    </x-primary-button>

                    <x-primary-button>
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/trash-can.png') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Delete Account') }}</span>
                    </x-primary-button>
                </div>
            </form>
        </div>
    </x-modal>
</section>
