<section>
    <header>
        <h2>{{ __('Update Password') }}</h2>
        @if (auth()->user() && auth()->user()->google_id)
            <p>{{ __('You are logged in using Google. Password management is handled externally.') }}</p>
        @else
            <p>{{ __('Ensure your account is using a long, random password to stay secure.') }}</p>
        @endif
    </header>

    @if (!auth()->user() || (auth()->user() && is_null(auth()->user()->google_id)))
        <div class="form-wrapper">
            <form method="post" action="{{ route('password.update') }}" class="default-form">
                @csrf
                @method('put')

                <div class="input-wrapper">
                    <x-input-label for="update_password_current_password" :value="__('Current Password')" />
                    <x-text-input id="update_password_current_password" name="current_password" type="password" autocomplete="current-password" />
                </div>

                <div class="input-wrapper">
                    <x-input-label for="update_password_password" :value="__('New Password')" />
                    <x-text-input id="update_password_password" name="password" type="password" autocomplete="new-password" />
                </div>

                <div class="input-wrapper">
                    <x-input-label for="update_password_password_confirmation" :value="__('Confirm Password')" />
                    <x-text-input id="update_password_password_confirmation" name="password_confirmation" type="password" autocomplete="new-password" />
                </div>

                <div class="button-wrapper">
                    <x-primary-button>{{ __('Save') }}</x-primary-button>
                </div>
            </form>
        </div>
    @endif

</section>
