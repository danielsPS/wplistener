<section>
    <header>
        <h2>{{ __('Profile Information') }}</h2>
        <p>{{ __("Update your account's profile information and email address.") }}</p>
    </header>

    <div class="form-wrapper">
        <form id="send-verification" method="post" action="{{ route('verification.send') }}">
            @csrf
        </form>

        <form method="post" action="{{ route('profile.update') }}" class="default-form" enctype="multipart/form-data">
            @csrf
            @method('patch')

            <div class="input-wrapper">
                <x-input-label for="profile_picture" :value="__('Profile Picture')" />

                <div class="profile-input-wrapper">
                    @if (Auth::user()->profile_picture)
                        <x-image-component
                            src="{{ asset('storage/' . Auth::user()->profile_picture) }}"
                            height="105"
                            width="105"
                            wrapper-class="profile-picture js-add-profile-picture"
                        />
                    @else
                        <x-image-component
                            src="{{ asset('assets/images/user.png') }}"
                            height="105"
                            width="105"
                            wrapper-class="profile-picture js-add-profile-picture"
                        />
                    @endif


                    <input type="file" id="profile_picture" name="profile_picture">
                    <input type="hidden" name="default_picture" id="default_picture" value="{{ asset('assets/images/user.png') }}">

                    <label class="remove-picture-wrapper js-remove-profile-picture">
                        <input type="checkbox" name="remove_picture" id="remove_picture" value="" style="visibility: hidden">
                        <x-image-component
                            src="{{ asset('assets/icons/close.svg') }}"
                            height="10"
                            width="10"
                            alt="Remove Picture"
                        />
                    </label>
                </div>
            </div>


            <div class="input-wrapper">
                <x-input-label for="name" :value="__('Name')" />
                <x-text-input id="name" name="name" type="text" :value="old('name', $user->name)" required autofocus autocomplete="name" />
            </div>

            <div class="input-wrapper">
                <x-input-label for="email" :value="__('Email')" />
                <x-text-input id="email" name="email" type="email" :value="old('email', $user->email)" required autocomplete="username" />

                @if ($user instanceof \Illuminate\Contracts\Auth\MustVerifyEmail && ! $user->hasVerifiedEmail())
                    <div class="must-verify-email-wrapper">
                        <p>
                            {{ __('Your email address is unverified.') }}

                            <button form="send-verification" class="underline text-sm text-gray-600 hover:text-gray-900 rounded-md focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                {{ __('Click here to re-send the verification email.') }}
                            </button>
                        </p>
                    </div>
                @endif
            </div>

            <div class="button-wrapper">
                <x-primary-button>{{ __('Save') }}</x-primary-button>
            </div>
        </form>
    </div>
</section>
