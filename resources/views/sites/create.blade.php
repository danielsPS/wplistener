@extends('dashboard')

@section('content')
    <div class="add-new-site-section">
        <h2>{{__('Add new site')}}</h2>

        @include('sites.partials.site-form', ['mode' => 'add'])
    </div>
@endsection
