@extends('dashboard')

@section('content')
    <div class="delete-site-section">
        <h2>{{__('Delete site')}}</h2>

        <div class="section-message-wrapper">
            <h3>{{ sprintf(__('Are you sure you want to delete %s?'), $site->name) }}</h3>

            <form method="POST" action="{{ route('sites.destroy', $site->id) }}">
            @csrf
            @method('DELETE')

            <!-- Submit -->
                <div class="button-wrapper">
                    <a href="{{ route('sites.edit', $site) }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/arrow-left.png') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Go back') }}</span>
                    </a>

                    <x-primary-button>
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/trash-can.png') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Delete site') }}</span>
                    </x-primary-button>
                </div>
            </form>
        </div>
    </div>
@endsection
