@extends('dashboard')

@section('content')
    <div class="add-new-site-section">
        <h2>{{__('Edit site')}}</h2>

        @include('sites.partials.site-form', ['mode' => 'edit'])
    </div>
@endsection
