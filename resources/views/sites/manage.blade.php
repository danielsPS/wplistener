@extends('dashboard')

@section('content')
    <div class="sites-list-section">
        <h2>
            <span>{{__('Manage Sites')}}</span>
            <span class="sites-count">({{count($sites)}})</span>
        </h2>

        @if(count($sites) > 0 || ($request->filled('site_url') || $request->filled('site_group') || $request->filled('site_tags')))
            <!-- Filter form -->
            @include('sites.partials.filter-form', ['fields' => ['url', 'group', 'tags'], 'route' => 'sites.manage'])

            <!-- Sites table -->
            <div class="table-wrapper sites-table-wrapper w-full">
                <table class="sites-table w-full">
                    <thead>
                        <tr>
                            <th class="col-services">{{__('Services')}}</th>
                            <th class="col-url">{{__('URL')}}</th>
                            <th class="col-group">{{__('Group')}}</th>
                            <th class="col-tags">{{__('Tags')}}</th>
                            <th class="col-edit"></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($sites as $site)
                        <tr>
                            <td class="col-services">
                                <div class="service-icons-wrapper">
                                    @if($site->services_uptime || $site->services_ssl)
                                        @if ($site->services_uptime)
                                            <div class="service-icon">
                                                <div class="service-icon-message">
                                                    {{__('Uptime monitoring')}}
                                                </div>
                                                <x-adaptive-svg url="{{asset('assets/icons/service-uptime.svg')}}" />
                                            </div>
                                        @endif
                                        @if ($site->services_ssl)
                                            <div class="service-icon">
                                                <div class="service-icon-message">
                                                    {{__('SSL monitoring')}}
                                                </div>
                                                <x-adaptive-svg url="{{asset('assets/icons/service-ssl-health.svg')}}" />
                                            </div>
                                        @endif
                                    @else
                                        <div class="not-applicable">N/A</div>
                                    @endif
                                </div>
                            </td>
                            <td class="col-url">
                                <a href="{{ $site->url }}" target="_blank">
                                    {{ $site->url }}
                                </a>
                            </td>
                            <td class="col-group">
                                {{ $site->group }}
                            </td>
                            <td class="col-tags">
                                @foreach ($site->tags as $tag)
                                    <span class="tag js-tag-color" data-tag-name="{{ $tag->name }}">{{ $tag->name }}</span>
                                @endforeach
                            </td>
                            <td class="col-edit">
                                <a href="{{ route('sites.edit', $site->id) }}">
                                    <x-adaptive-svg url="{{asset('assets/icons/edit.svg')}}" />
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @if(count($sites) <= 0)
                <div class="section-message-wrapper mt-4">
                    <h3>{{ __('No sites found matching your criteria.') }}</h3>
                </div>
            @endif

        @else
            <div class="section-message-wrapper">
                <h3>{{ __('There are no sites. Please add one.') }}</h3>

                <div class="button-wrapper">
                    <a href="{{ route('sites.create') }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/add-new.svg') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Add new site') }}</span>
                    </a>
                </div>
            </div>
        @endif

    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const tags = document.querySelectorAll('.js-tag-color');
            tags.forEach(function(tag) {
                const tagName = tag.getAttribute('data-tag-name');
                tag.style.backgroundColor = tagName.toColor();
            });
        });
    </script>
@endsection

