@php
    $route = $route ?? 'sites.manage';
@endphp

<div class="form-wrapper filter-form-wrapper">
    <form method="GET" action="{{ route($route) }}" class="filter-form">
        @if(in_array('url', $fields))
            <!-- URL -->
            <label class="input-wrapper">
                <x-text-input type="text" name="site_url" :value="request('site_url')" placeholder="{{__('Url')}}" />
            </label>
        @endif

        @if(in_array('status', $fields))
            <!-- Status -->
            <label class="select-wrapper">
                <select name="site_status">
                    <option value="">{{ __('Status') }}</option>
                    <option value="1">{{ __('Online') }}</option>
                    <option value="0">{{ __('Offline') }}</option>
                </select>
            </label>
        @endif

        @if(in_array('group', $fields))
            <!-- Group -->
            <label class="select-wrapper">
                <select name="site_group">
                    <option value="">{{ __('Group') }}</option>
                    @foreach ($groups as $group)
                        <option value="{{ $group }}" {{ (request('site_group') == $group) ? 'selected' : '' }}>{{ $group }}</option>
                    @endforeach
                </select>
            </label>
        @endif

        @if(in_array('tags', $fields))
            <!-- Tags -->
            <label class="select-wrapper">
                <select name="site_tags">
                    <option value="">{{ __('All tags') }}</option>
                    @foreach ($tags as $tag)
                        <option value="{{ $tag->id }}" {{ (request('site_tags') == $tag->id) ? 'selected' : '' }}>{{ $tag->name }}</option>
                    @endforeach
                </select>
            </label>
        @endif

        <!-- Submit -->
        <div class="button-wrapper">
            <x-primary-button>
                {{ __('Filter sites') }}
            </x-primary-button>

            @if($request->filled('site_url') || $request->filled('site_group') || $request->filled('site_tags'))
                <a href="{{ route($route) }}" class="clear-filter-button">
                    <span>{{ __('Clear Filter') }}</span>
                    <x-adaptive-svg url="{{asset('assets/icons/close.svg')}}" height="14" width="14" />
                </a>
            @endif
        </div>
    </form>
</div>
