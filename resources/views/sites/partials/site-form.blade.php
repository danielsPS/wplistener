@php
    $site = $site ?? new \App\Models\Site;
@endphp

<div class="form-wrapper">
    <form method="POST" action="{{ $mode === 'edit' ? route('sites.update', $site) : route('sites.store') }}" class="default-form">
        @csrf
        @if($mode === 'edit')
            @method('PUT')
        @endif

        <!-- Site name -->
        <div class="input-wrapper">
            <x-input-label for="site_name" :value="__('Site name')" />
            <x-text-input id="site_name" type="text" name="site_name" value="{{ old('site_name', $site->name) }}" />
        </div>

        <!-- URL -->
        <div class="input-wrapper">
            <x-input-label for="site_url" :value="__('URL')" />
            <x-text-input id="site_url" type="text" name="site_url" value="{{ old('site_url', $site->url) }}" />
        </div>


        <!-- Group name -->
        <div class="select-wrapper">
            <label for="site_group">{{ __('Group name') }}</label>
            <select class="select2-tags" name="site_group" id="site_group">
                @foreach($groups as $group)
                    <option value="{{ $group }}" {{ $group == $site->group ? 'selected' : '' }}>{{ $group }}</option>
                @endforeach
            </select>
        </div>

        <!-- Tags -->
        <div class="select-wrapper">
            <label for="site_tags">{{ __('Tags') }}</label>
            <div class="tag-display-area js-tag-display-area"></div>
            <select class="select2-tags select2-tags-multiple" name="site_tags[]" id="site_tags" multiple="multiple">
                @foreach($tags as $tag)
                    <option value="{{ $tag->name }}" {{ $site->tags->contains($tag->id) ? 'selected' : '' }}>{{ $tag->name }}</option>
                @endforeach
            </select>
        </div>

        <!-- Services -->
        <div class="checkbox-wrapper">
            <label>{{ __('Services') }}</label>

            <label class="checkbox-label">
                <input type="checkbox" name="services_uptime" id="services-uptime" value="1" {{ $site->services_uptime ? 'checked' : '' }} style="visibility: hidden">
                <span class="checkbox">
                    <span class="checkbox-text">
                        <img src="{{ asset('assets/icons/service-uptime.svg') }}" alt="" width="23" height="23">
                        <span>{{ __( 'Uptime monitoring' ) }}</span>
                    </span>
                </span>
            </label>

            <label class="checkbox-label">
                <input type="checkbox" name="services_ssl" id="services-ssl" value="1" {{ $site->services_ssl ? 'checked' : '' }} style="visibility: hidden">
                <span class="checkbox">
                    <span class="checkbox-text">
                        <img src="{{ asset('assets/icons/service-ssl-health.svg') }}" alt="" width="23" height="23">
                        <span>{{ __( 'SSL monitoring' ) }}</span>
                    </span>
                </span>
            </label>
        </div>

        <!-- Submit -->
        <div class="button-wrapper">
            <x-primary-button>
                <x-image-component
                    src="{{ asset('assets/icons/navigation/add-new.svg') }}"
                    alt=""
                    height="16"
                    width="16"
                />
                <span>{{ $mode === 'edit' ? __('Update site') : __('Add new site') }}</span>
            </x-primary-button>

            @if($mode === 'edit')
                <a href="{{ route('sites.delete', $site) }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                    <x-image-component
                        src="{{ asset('assets/icons/navigation/trash-can.png') }}"
                        alt=""
                        height="16"
                        width="16"
                    />
                    <span>{{ __('Delete site') }}</span>
                </a>
            @endif
        </div>
    </form>
</div>
