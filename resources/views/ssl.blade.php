@php
    use Carbon\Carbon;
@endphp

<x-app-layout>
    <div class="ssl-section service-section default-section-layout">
        {{-- Header--}}
        <header class="accounts-page-header">
            <h2>{{ __('SSL Health') }}</h2>
            @include('components.notifications')
        </header>

        @if(count($sites) > 0 || ($request->filled('site_url') || $request->filled('site_status') || $request->filled('site_tags')))
        <!-- Filter form -->
            @include('sites.partials.filter-form', ['fields' => ['url', 'group', 'tags'], 'route' => 'ssl.manage'])

        <!-- Sites table -->
            <div class="table-wrapper ssl-table-wrapper service-table-wrapper w-full">
                <table class="ssl-table service-table w-full">
                    <thead>
                        <tr>
                            <th class="col-site">{{__('Site')}}</th>
                            <th class="col-url">{{__('URL')}}</th>
                            <th class="col-expiry">{{__('Expiry Date')}}</th>
                            <th class="col-issuer">{{__('Issuer')}}</th>
                            <th class="col-till-expiry">{{__('Days Until Expiry')}}</th>
                            <th class="col-group">{{__('Group')}}</th>
                            <th class="col-tags">{{__('Tags')}}</th>
                            <th class="col-edit"></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($sites as $site)
                        <tr>
                            <td class="col-site">
                                <h3>{{ $site->name }}</h3>
                            </td>
                            <td class="col-url">
                                <a href="{{ $site->url }}" target="_blank">
                                    {{ $site->url }}
                                </a>
                            </td>
                            <td class="col-expiry">
                                @if ($site->sslCertificate)
                                    {{ $site->sslCertificate->expiry_date }}
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-issuer">
                                @if ($site->sslCertificate)
                                    {{ $site->sslCertificate->issuer }}
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-till-expiry">
                                @if ($site->sslCertificate)
                                    @php
                                        $daysUntilExpiry = $site->getDaysUntilSslExpiryDate();
                                    @endphp
                                    @if ($daysUntilExpiry > 0)
                                        <div class='till-active'>{{$daysUntilExpiry}} {{__('days')}}</div>
                                    @else
                                        <div class='till-expired'>{{__('Expired')}}</div>
                                    @endif
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-group">
                                {{ $site->group }}
                            </td>
                            <td class="col-tags">
                                @foreach ($site->tags as $tag)
                                    <span class="tag js-tag-color" data-tag-name="{{ $tag->name }}">{{ $tag->name }}</span>
                                @endforeach
                            </td>
                            <td class="col-edit">
                                <a href="{{ route('sites.edit', $site->id) }}">
                                    <x-adaptive-svg url="{{asset('assets/icons/edit-2.svg')}}" />
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @if(count($sites) > 0)
                <div class="run-site-check-button-wrapper">
                    <a href="{{ route('ssl.check') }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <span>{{ __('Run Site Check') }}</span>
                    </a>
                </div>
            @endif

            @if(count($sites) <= 0)
                <div class="section-message-wrapper mt-4">
                    <h3>{{ __('No sites found matching your criteria.') }}</h3>
                </div>
            @endif

        @else
            <div class="section-message-wrapper">
                <h3>{{ __('There are no sites where SSL check is enabled. Please go to your sites and enable it.') }}</h3>

                <div class="button-wrapper">
                    <a href="{{ route('sites.manage') }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/sites-white.svg') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Manage Sites') }}</span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const tags = document.querySelectorAll('.js-tag-color');
            tags.forEach(function(tag) {
                const tagName = tag.getAttribute('data-tag-name');
                tag.style.backgroundColor = tagName.toColor();
            });
        });
    </script>
</x-app-layout>
