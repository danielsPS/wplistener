@php
    use Carbon\Carbon;
@endphp

<x-app-layout>
    <div class="uptime-section service-section default-section-layout">
        {{-- Header--}}
        <header class="accounts-page-header">
            <h2>{{ __('Uptime monitoring') }}</h2>
            @include('components.notifications')
        </header>

        @if(count($sites) > 0 || ($request->filled('site_url') || $request->filled('site_status') || $request->filled('site_tags')))
            <!-- Filter form -->
            @include('sites.partials.filter-form', ['fields' => ['url', 'status', 'tags'], 'route' => 'uptime.manage'])

            <!-- Sites table -->
            <div class="table-wrapper uptimes-table-wrapper service-table-wrapper w-full">
                <table class="uptimes-table service-table w-full">
                    <thead>
                        <tr>
                            <th class="col-site">{{__('Site')}}</th>
                            <th class="col-url">{{__('URL')}}</th>
                            <th class="col-status">{{__('Status')}}</th>
                            <th class="col-availability">{{__('Availability')}}</th>
                            <th class="col-downtime">{{__('Downtime')}}</th>
                            <th class="col-response-time">{{__('Avg response time')}}</th>
                            <th class="col-graph">{{__('Response time graph (last 24h)')}}</th>
                            <th class="col-edit"></th>
                        </tr>
                    </thead>

                    <tbody>
                    @foreach ($sites as $site)
                        <tr>
                            <td class="col-site">
                                <h3>{{ $site->name }}</h3>
                            </td>
                            <td class="col-url">
                                <a href="{{ $site->url }}" target="_blank">
                                    {{ $site->url }}
                                </a>
                            </td>
                            <td class="col-status">
                                @if($site->uptime)
                                    @if ($site->uptime->status)
                                        <x-image-component src="{{ asset('assets/icons/status-online.svg') }}" alt="{{ __('Online') }}" height="21" width="21"/>
                                    @else
                                        <x-image-component src="{{ asset('assets/icons/status-offline.svg') }}" alt="{{ __('Offline') }}" height="21" width="21"/>
                                    @endif
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-availability">
                                @if($site->uptime)
                                    {{ $site->getFormattedAvailability() }}
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-downtime">
                                @if($site->uptime)
                                    {{ $site->getFormattedDowntime() }}
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-response-time">
                                @if($site->uptime)
                                    {{ $site->getFormattedAverageResponseTime() }}
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-graph">
                                @if($site->uptime)
                                    <div class="response-graph">
                                        <canvas id="responseTimeChart{{ $site->id }}" height="21" width="191"></canvas>
                                    </div>
                                    <div class="uptime-graph">
                                        @foreach($site->getUptimeStatusForLast24Hours() as $status)
                                            <div class="hour-cube {{ $status ? 'positive' : 'negative' }}"></div>
                                        @endforeach
                                    </div>
                                @else
                                    <div class="not-applicable">N/A</div>
                                @endif
                            </td>
                            <td class="col-edit">
                                <a href="{{ route('sites.edit', $site->id) }}">
                                    <x-adaptive-svg url="{{asset('assets/icons/edit-2.svg')}}" />
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>

            @if(count($sites) > 0)
                <div class="run-site-check-button-wrapper">
                    <a href="{{ route('uptime.check') }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <span>{{ __('Run Site Check') }}</span>
                    </a>
                </div>
            @endif

            @if(count($sites) <= 0)
                <div class="section-message-wrapper mt-4">
                    <h3>{{ __('No sites found matching your criteria.') }}</h3>
                </div>
            @endif

        @else
            <div class="section-message-wrapper">
                <h3>{{ __('There are no sites where uptime is enabled. Please go to your sites and enable it.') }}</h3>

                <div class="button-wrapper">
                    <a href="{{ route('sites.manage') }}" class="primary-button inline-flex items-center justify-center font-bold text-white transition ease-in-out duration-150">
                        <x-image-component
                            src="{{ asset('assets/icons/navigation/sites-white.svg') }}"
                            alt=""
                            height="16"
                            width="16"
                        />
                        <span>{{ __('Manage Sites') }}</span>
                    </a>
                </div>
            </div>
        @endif
    </div>

    <script>
        document.addEventListener('DOMContentLoaded', function() {
            const chartDataArray = [];

            @foreach ($sites as $site)
            chartDataArray.push({
                id: 'responseTimeChart{{ $site->id }}',
                data: {
                    labels: [...Array(24).keys()].map(hour => `${hour}:00`),
                    data: @json($site->getAverageResponseTimeForLast24Hours()),
                    label: 'Average Response Time'
                }
            });
            @endforeach

            chartDataArray.forEach(chartInfo => {
                loadChart(chartInfo.id, chartInfo.data);
            });
        });
    </script>

</x-app-layout>
