<?php

use App\Http\Controllers\SiteController;
use App\Http\Controllers\GoogleController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SSLCertificateController;
use App\Http\Controllers\UptimeController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
})->middleware('guest');


// Site management routes
Route::get('/dashboard', [SiteController::class, 'index'])->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/sites', function () {
        return redirect()->route('sites.manage');
    });

    Route::prefix('sites')->group(function () {
        Route::get('/manage', [SiteController::class, 'manage'])->name('sites.manage');
        Route::get('/create', [SiteController::class, 'create'])->name('sites.create');
        Route::get('/edit/{site}', [SiteController::class, 'edit'])->name('sites.edit');
        Route::get('/delete/{site}', [SiteController::class, 'delete'])->name('sites.delete');
        Route::get('/import', [SiteController::class, 'import'])->name('sites.import');

        Route::post('/store', [SiteController::class, 'store'])->name('sites.store');
        Route::put('/update/{site}', [SiteController::class, 'update'])->name('sites.update');
        Route::delete('/destroy/{site}', [SiteController::class, 'destroy'])->name('sites.destroy');

        // Fallback route for any other /sites/* requests
        Route::fallback(function () {
            return redirect()->route('sites.manage');
        });
    });
});

// Uptime routes
Route::middleware('auth')->group(function () {
    Route::get('/uptime', function () {
        return redirect()->route('uptime.manage');
    });

    Route::prefix('uptime')->group(function () {
        Route::get('/', [UptimeController::class, 'index'])
            ->name('uptime.manage')
            ->middleware('verified');

        Route::get('/check', [UptimeController::class, 'check'])
            ->name('uptime.check');
    });
});

// SSL routes
Route::middleware('auth')->group(function () {
    Route::prefix('ssl')->group(function () {
        Route::get('/', [SSLCertificateController::class, 'index'])
            ->name('ssl.manage')
            ->middleware('verified');

        Route::get('/check', [SSLCertificateController::class, 'check'])
            ->name('ssl.check');
    });
});


// Profile routs
Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});


// Google login
Route::prefix('login/google')->group(function () {
    Route::get('/', [GoogleController::class, 'redirectToGoogle'])->name('login.google');
    Route::get('/callback', [GoogleController::class, 'handleGoogleCallback']);
});


require __DIR__.'/auth.php';
